# Processed data files description #

This file contains description of the processed data files. These are all csv files, with column names in headers and without row names.

## slopes.csv ##

This file contains froup and individual slopes, one row per session. The columns are:

* group_slope - The slope parameter for the group (fitted to group decisions).
* slope_i (for i = from 1 to 4) - The slope parameter of the i-th participant (the participant index depends on the workstation used).

## sessions.csv ##

This file contains group-level data (e.g. parameters, dates). The columns are:

* session_id - Id of the session.
* n_ppants - Number of participants in this session (4 for this study).
* datetime - Date and time the session started.
* filename - Name of the raw data file containing the session's data.
* duration_sec - Duration of the session in seconds.
* slope - The fitted slope parameter of the group.
* bias - The fitted slope parameter of the group.
* fit.ok - FALSE if there was a problem fitting the model (includes all fit.* flags).
* fit.converged - TRUE if glm.fit reported covnergence.
* fit.sep.ok - FALSE if there was a separation issue.
* fit.hl.test.p - p-value of the Hosmer-Lemeshaw test for lack-of-fit.
* slope.mean - Average member slope.
* slope.min - Minumum member slope.
* slope.max - Maximum member slope.
* slope.mean.sq - Mean of squares of member slopes.
* slope.var - Variance of member slopes in a group (NOTE: actual population variance, not an estimator).
* acc - Group's overall accuracy  (no. correct answers/ no. trials).
* balance - Overall of proportion of group answers equal to '1' for this group.
* acc.maker - Overall accuracy of the answers made by participants inputting the group decisions.
* acc.mean - Average member accuracy in the group.
* acc.min - Minimum member accuracy in the group.
* acc.max - Maximum member accuracy in the group.
* agree.maker - Proportion of trials in which the group decision maker's decision was the same as the group decision.
* knows_min - Minimum level of familiarity between any two members of a group (scale 1 - 4).
* knows_max - Maximum level of familiarity between any two members of a group (scale 1 - 4).
* familiarity - The overall group familiarity (see the main report).

## ppants.csv ##

This file contains per-participant data (e.g. parameters, demographics). The columns are:

* session_id - Id of the session.
* ppant_id - Global identifier of the participant.
* ppant_index - Index of the participant (within a session, from 1 to n_ppants).
* client - Id of the client prgogram (workstation).
* sex - Sex of the participant.
* age - Age of the participant.
* edu - Participant's level of education (see main report).
* knows_left - Participant's stated level of familiarity with the person sitting to the left.
* knows_front - Participant's stated level of familiarity with the person sitting in front of her/him.
* knows_right - Participant's stated level of familiarity with the person sitting to the right.
* ppant_id_left - Identifier of the person sitting to the left of the participant.
* ppant_id_front - Identifier of the person sitting in front of the participant.
* ppant_id_right - Identifier of the person sitting to the right of the participant.
* slope - The fitted slope parameter of the participant.
* bias - The fitted slope parameter of the participant.
* fit.ok - FALSE if there was a problem fitting the model (includes all fit.* flags).
* fit.converged - TRUE if glm.fit reported covnergence.
* fit.sep.ok - FALSE if there was a separation issue.
* fit.hl.test.p - p-value of the Hosmer-Lemeshaw test for lack-of-fit.
* acc - Group's overall accuracy  (no. correct answers/ no. trials).
* balance - Overall of proportion of group answers equal to '1' for this group.
* slope.rank - Participant's rank (within the group) by the slope parameter. Higher rank means higher value.
* bias.rank - Participant's rank (within the group) by the bias parameter. Higher rank means higher value.
* acc.rank - Participant's rank (within the group) by accuracy. Higher rank means higher value.
* balance.rank - Participant's rank (within the group) by the balance (proportion of answers = '1'). Higher rank means higher value.
* n_wins - Number of trials in which this participant's decision was the same as the group decision.
* n_loses - Number of trials in which this participant's decision was different from the group decision.
* agree - Proportion of trials in which this participant's decision was the same as the group decision.

## ppairs.csv ##

This file contains data about (unordered) pairs of participants (stats on concordant decisions). The columns are:

* session_id - Id of the session.
* ppant_id.A - Lower of the two id's of the two participants in this pair.
* ppant_id.B - Higher of the two id's of the two participants in this pair.
* agree - Proportion of trials in which the two participants made the same decision.
* n_agree - Number of trials in which the two participants made the same decision.
* n_disagree - Number of trials in which the two participants made different decisions.

## trials.csv ##

This file contains per-trial data (e.g. contrast, group decisions, decision distribution). The columns are:

* session_id - Id of the session.
* trial_id - Global identifier of the trial.
* trial_no - Trial number (within a session).
* experimental - TRUE if the trial was experimental, FALSE for practice trials.
* target_pos - Position of the target stimulus in the set (see experiment description).
* contrast_idx - Index of the contrast value used for the target.
* contrast_diff - Contrast difference value in this trial.
* correct_dec - Correct answer or this trial (0 or 1).
* group_dec_maker - Id of the participant inputing the group decision.
* group_dec - Decision (answer) of the group.
* group_dec_correct - Was the group decision correct?
* group_dec_rt - Duration of the group decision-making process (until the button was pressed) in seconds.
* abs_contrast_diff - Absolute value of contrast difference in this trial.
* group_dec_1 - Was the group decision equal to 1? (boolean)
* group_dec_maker - Id of the participant inputting the group decision in this trial (or NA).
* maker_dec_correect - Was the individual maker's decision correct?
* maker_dec_wins - Was the group decision equal to the maker's decision.
* maker_dec - Individual decision of this trial group decision maker (*not* the group decision).
* dec_dist - Number of the minority decision in the group: 0 (unanimity), 1,or 2 (tie).
* decs - Individual decisions pasted together as string, orederd by ppant_id, e.g. 0, 1, 0, 1 -> '0101'
* decs_rp0 - Individual decisions relative to first members decison (TRUE if equal), e.g. 0, 1, 0, 1 -> 'FTF'.
* dec_p0 - Decision of the first participant
* group_dec_rp0 - Group decision reliative to the first participant (TRUE if equal).

## decisions.csv ##

This file contains individual decision data, one per participant per decision. The columns are:

* session_id - Id of the session.
* trial_id - Global identifier of the trial.
* trial_no - Trial number (within a session).
* ppant_id - Id of the participant making the decision.
* ppant_index - Index of the participant making the decision (within a session, from 1 to n_ppants).
* dec - The decision: 0 or 1, meaning that the target was observed in the first or second set of stimuli, respectively.
* correct - Is the decision (answer) correct?
* rt - Decision time (time from the offset of the second stimuli set to the moment the button was pressed).
