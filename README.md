# README #

This repository contains data, R scripts, and knitr report templates for the "Interacting minds - groups of four" experiment, conducted by the Institute of Psychology, Polish Academy of Sciences (2014).
The relevant articles, explaining the experiment and methodology, are mentioned in the bibliography.

### Data ###

Processed data is in the 'processed' directory - this is what you probably want. It contains following files:

* slopes.csv - Group and individual slopes, one row per session - the bare minimum.
* sessions.csv - Group-level data (e.g. parameters, dates).
* ppants.csv - Per-participant data (e.g. parameters, demographics)
* ppairs.csv - Data about (unordered) pairs of participants (stats on concordant decisions).
* trials.csv - Per-trial data (e.g. contrast, group decisions, decision distribution)
* decisions.csv - Individual decision data, one per participant per decision.

To recalculate all processed data use "make processed".

TODO: column descriptions

Raw data (one file per session) is contained in the 'raw' directory.

### Scripts ###

Scripts require R (v >= 3.1) and some publicly available packages (most notably 'data.table').

The purpose of each scrpit:

* analyses.R - Runs main analysis and stores the result in the environment (there is no make target, you need to source this file).
* nminds.R - Functions for processing and analysing the data.
* read.R - Functions for reading raw data and converting it ot R-friendly form.
* simulate.R - Functions for simulating group behaviour using theoretical model [work in progress].
* utils.R - Some utilities

See comments inside scripts for more info.

To run test use "make test" [work in progress]. Tests use 'testthat' package.

### Reports ###

Reports use knitr and LaTex (also beamer).
To compile them use "make" (default target). The 'cache' and 'figure' directories are used to store intermediate data.
Final build results (.pfd) will be copied to 'build' direcotry in the project root.

Reports:

* rap-nminds-x4-2014.Rnw - Main report with summary analyses.
* supp-nminds-x4-2014.Rnw - Supplementary report with per-session data.
* nminds-prez-x4-zako2014.Rnw - Presentation for the 4th Peripatetic conference (in polish).

To remove all output nad intermediate files, use "make clean".

### Contact ###

Author and maintainer:

* Michał Denkiewicz (michal.denkiewicz@gmail.com)

### Bibliography ###

* Piotr Migdał, Joanna Rączaszek-Leonardi, Michał Denkiewicz, Dariusz Plewczynski, Information-sharing and aggregation models for interacting minds, *Journal of Mathematical Psychology*, Volume 56, Issue 6, December 2012, Pages 417-426, ISSN 0022-2496, http://dx.doi.org/10.1016/j.jmp.2013.01.002 (http://www.sciencedirect.com/science/article/pii/S0022249613000096)
* Denkiewicz, M., Rączaszek-Leonardi, J., Migdał, P., Plewczyński, D., (2013). Information-Sharing in Three Interacting Minds Solving a Simple Perceptual Task. *In M. Knauff, M. 	Pauen, N. Sebanz, & I. Wachsmuth (Eds.), Proceedings of the 35th Annual Conference of the Cognitive Science Society* (pp. 2172-2177). Austin, TX: Cognitive Science Society.