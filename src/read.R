#####################################################################################################
##
## Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)
##
## This script is a part of a set of scripts for the "Interacting minds - groups of four" experiment,
## conducted by the Institute of Psychology, Polish Academy of Sciences (2014).
##
## This file contains functions for reading raw data and converting it ot R-friendly form.
##
#####################################################################################################

library(stringr)
library(data.table)
library(dplyr)

# Directory containing raw data files producede by the expierimental program
RAW.DATA.DIR = '../raw/'

source('utils.R')

#'
#' Extracts the number of participants per group from the raw data file.
#'
#' @param X Raw data file.
#'
#' @return number of participants in all groups in this file.
#' 
get.n.ppants = function(X) {
    sum(str_detect(colnames(X), '^p(\\d+)\\.indiv_dec$'))
}

#'
#' Gets a given column pertaining to the i-th participant
#'
#' @param data Raw data (as data.table).
#' @param colname Name of the column to extract.
#' @param i Index of the participant.
#'
#' @return The extracted column, as a vector.
#' 
get.ppant.col = function(data, colname, i) {
    if(length(colname) == 1) {
        data[[sprintf('p%d.%s', i, colname)]]
    } else {
        data[,sprintf('p%d.%s', i, colname), with=FALSE]
    }
}

#'
#' Reads a raw data file, produced by the experimental program.
#' Each data file contains information about a single session.
#' Survey data (demographics) does not reside these files, and has to be loaded separately.
#'
#' Returns a list containing four tables: session, ppants, trials and decisions.
#' These contain information stored at session (group), participant, trial and
#' decision level respectively.
#'
#' Columns provided (see experiment description for more details):
#'
#' session:
#' session_id - Identifier of the session (character type).
#' n_ppants - Number of particpants in this session.
#' datetime - Date and time the session started (POSIXct type).
#' filename - Name of the raw data file.
#' duration_sec - Duration of the session in seconds.
#'
#' ppants:
#' ppant_id - Global identifier of the participant.
#' ppant_index - Index of the participant within this session.
#' session_id - Identifier of the session.
#'
#' trials:
#' trial_id - Global identifier of the trial.
#' trial_no - Trial number (within a session).
#' session_id - Identifier of the session.
#' experimental - TRUE if the trial was experimental, FALSE for practice trials.
#' target_pos - Position of the target stimulus in the set (see experiment description).
#' contrast_idx - Index of the contrast value used for the target.
#' contrast_diff - Contrast difference value in this trial.
#' abs_contrast_diff - Absolute value of the contrast difference in this trial.
#' correct_dec - Correct answer or this trial (0 or 1).
#' group_dec_maker - Id of the participant inputing the group decision.
#' group_dec - Decision (answer) of the group.
#' group_dec_correct - Was the group decision correct?
#' group_dec_rt - Duration of the group decision-making process (until the button was pressed) in seconds.
#'
#' decisions:
#' session_id - Identifier of the session.
#' trial_id - Global identifier of the trial, in which the decision was made.
#' trial_no - Trial number.
#' ppant_id - Global identifier of the participant making the decision.
#' ppant_index - Index of the participant (within a session) making the decision.
#' dec - The decision: 0 or 1, meaning that the target was observed in the first or second set of stimuli, respectively.
#' correct - Is the decision (answer) correct?
#' rt - Decision time (time from the offset of the second stimuli set to the moment the button was pressed).
#'
#' @param path Path to the file to read.
#' @param timinf Include stimulus timing information?
#'
#' @return Data from the file as a list of four tablea (see above).
#' 
read.data.file = function(path, timing=FALSE) {
    data = fread(path, header=T, sep='\t')

    ma = str_match(path, 'nminds-exp-(\\d{8}-\\d{4})-(\\d{3})\\.txt')
    
    n_ppants = get.n.ppants(data)
    session_id = ma[3]
    session = data.table(
        session_id = session_id,
        n_ppants = n_ppants,
        datetime = as.POSIXct(strptime(ma[2], '%Y%m%d-%H%M')),
        filename = ma[1],
        duration_sec = data$trial_finish[nrow(data)] - data$trial_start[1]
    )
    
    trials = data.table(
        trial_id = sprintf(str_c(session_id, "-%04d"), 1:nrow(data)),
        trial_no = 1:nrow(data),
        session_id = rep(session_id, nrow(data)),
        experimental = as.logical(data$block_type == 'experimental'),
        target_pos = as.factor(data$target_pos),
        contrast_idx = as.factor(data$contrast_idx),
        contrast_diff = ifelse(data$target_screen_nr==1, data$contrast - CONTRAST.BASELINE, -data$contrast + CONTRAST.BASELINE),
        correct_dec = as.factor(data$target_screen_nr),
        group_dec_maker = ifelse(is.na(data$group_dec_maker), NA, sprintf('%s-p%d', session_id, data$group_dec_maker)),
        group_dec = as.factor(data$group_dec),
        group_dec_correct = data$group_dec == data$target_screen_nr,
        group_dec_rt = data$group_dec_rt
    )
    trials[,abs_contrast_diff := abs(contrast_diff)]
    if(timing) {
        trials[, trial_start := data$trial_start]
        trials[, trial_finish := data$trial_finish]
    }
    
    n = nrow(trials)
    if(timing) {
        ma = data.frame(str_match(colnames(data), '^p(\\d+)\\.((\\w+).(onset|offset|duration))$')[, -1], stringsAsFactors=FALSE)
        ma = ma[!is.na(ma[, 1]),]
        stim.cols = ma[ma[, 1] == '0', 2]
    }
    decisions = lapply(1:n_ppants, function(i) {
        dt = data.table(
            session_id = rep(session_id, n),
            trial_id = trials$trial_id,
            trial_no = trials$trial_no,
            ppant_id = rep(sprintf('%s-p%d', session_id, i - 1), n),
            ppant_index = rep(i, n),
            dec = as.factor(get.ppant.col(data, 'indiv_dec', i - 1)),
            correct = get.ppant.col(data, 'indiv_dec', i - 1) == trials$correct_dec,
            rt = get.ppant.col(data, 'indiv_dec_rt', i - 1)
        )
        if(timing) {
            dttimes = get.ppant.col(data, stim.cols, i - 1)
            setnames(dttimes, stim.cols)
            dt = cbind(dt, dttimes)
        }
        dt
    })

    ppants = data.table(
        ppant_id = sprintf('%s-p%d', session_id, 0:(n_ppants-1)),
        ppant_index = 1:n_ppants,
        session_id = session_id
    )
    
    list(session=session,
         trials=trials,
         decisions=rbindlist(decisions),
         ppants=ppants)
}

#'
#' Reads in the survey data from a .csv file. This single file contains info from surveys from the entire experiment. Each row both in the input and in the output contains information about one participant.
#'
#' Returns a data.table with following columns (see experiment description for more details)::
#' session_id - Identifier of the session.
#' client - Id of the client prgogram (workstation).
#' sex - Sex of the participant.
#' age - Age of the participant.
#' edu - Participant's level of education.
#' knows_left - Participant's stated level of familiarity with the person sitting to the left.
#' knows_front - Participant's stated level of familiarity with the person sitting in front of her/him.
#' knows_right - Participant's stated level of familiarity with the person sitting to the right.
#' ppant_id - Global identifier of the participant.
#' ppant_id_left - Identifier of the person sitting to the left of the participant.
#' ppant_id_front - Identifier of the person sitting in front of the participant.
#' ppant_id_right - Identifier of the person sitting to the right of the participant.
#' 
#' @param path Path to the file.
#' @param n.ppants Number of participants per session.
#'
#' @return A data.table with survey data (column description: see above).
#' 
read.survey = function(path, n.ppants=4) {
    X = fread(path)
    numclient = match(X$client, LETTERS[1:n.ppants])
    X$client = paste0('client-', X$client)
    X$session_id = sprintf('%03d', X$session_id)
    X$ppant_id = sprintf('%s-p%d', X$session_id, numclient - 1)
    X$ppant_id_left = sprintf('%s-p%d', X$session_id, (numclient + 3 - 1) %% n.ppants)
    X$ppant_id_front = sprintf('%s-p%d', X$session_id, (numclient + 2 - 1) %% n.ppants)
    X$ppant_id_right = sprintf('%s-p%d', X$session_id, (numclient + 1 - 1) %% n.ppants)
    X$edu = factor(X$edu, levels=1:6, labels=c('elem. sch.', 'high sch.', 'undergrad. st.', 'higher', 'grad. st.', 'PhD'))
    setkey(X, session_id, ppant_id)
    X    
}

#'
#' Reads all raw files from a folder creating processed data.tables for analysis,
#' rbinds them together. Reads and merges survey data from the same directory.
#'
#' Returns a list containing four tables: sessions, ppants, trials and decisions.
#' These contain information stored at session (group), participant, trial and
#' decision level respectively. These tables contain data from all data files
#' in the directory.
#'
#' @param directory Path to the directory in which data files reside.
#' @param pattern Pattern identifying data file names.
#'
#' @return A list with data.tables containing the periment data (see above).
#'
read.nminds.data = function(directory=RAW.DATA.DIR, pattern='nminds-exp', timing=FALSE) {
    .data.files = lapply(list.files(directory, pattern=pattern),
        function(filename) paste0(directory, filename))

    .data = lapply(.data.files, read.data.file, timing=timing)

    sessions = rbindlist(lapply(.data, '[[', 'session'))
    setkey(sessions, session_id)
    ppants = rbindlist(lapply(.data, '[[', 'ppants'))
    setkey(ppants, session_id, ppant_id)
    trials = rbindlist(lapply(.data, '[[', 'trials'))
    setkey(trials, session_id, trial_id)
    decisions = rbindlist(lapply(.data, '[[', 'decisions'))
    setkey(decisions, session_id, trial_id, ppant_id)

    .survey = read.survey(paste0(directory, 'nminds-x4-survey.csv'))
    ppants = merge(ppants, .survey)

    list(sessions=sessions, ppants=ppants, trials=trials, decisions=decisions)    
}
