# Raw data files description #

This file contains description of the raw data files (output from the experimental software) and the survey file (data about participants collected in the form of paper surveys).

## Experiment data files (nminds-...) ##

### Description ###

Each file is named nminds-exp-YYYYMMDD-hhmm-xxx.txt (Y - year, M - month, D - day, h - hour in 24h format, m - minute, x - session id).
The experimental files are UTF-8 encoded text files, with following conventions:

* row separator - Microsoft Windows new line separator (CRLF).
* cell separator - TAB ('\t')
* decimal separator - Dot ('.')
* missing values - R's NA as unquoted string ('NA')

The first line of each file contains column names.
The file contains one row per trial.

### Columns ###

* block_type –  Type of block - practice or experimental.
* no_blocks – Number of blocks of given type.
* practice – True for practice blocks.
* block_idx – Index (starting from 0) of this block (within its type).
* no_subblocks – Number of subblocks within this block.
* participants – Comma-separated list assigning participant numbers to workstations.
* subblock_idx - Index (starting from 0) of this subblock (within its block).
* trial_idx - Index (starting from 0) of this trial (within its subblock).
* target_pos – An integer from 0 to 5 designation the position of the target within the six-patch set, starting from the rightmost position, counterclockwise.
* target_screen_nr – Index of the screen (0 or 1), containing the target.
* contrast_idx – Index of the contrast value, from 1 (smallest) do 5 (largest).
* contrast – Contrast value (float).
* feedback_texts_order – Vertical order of the feedback texts for each participant (starting from the top).
* trial_start – Time (in seconds) from the start of the experiment to the start of this trial (i.e. onset of the blank screen before fisrt set of stimuli).
* group_dec_maker – Number (0-3) of the designated decision-maker (NA for skipping group decision phase).
* group_dec – Groud decision in this trial (0 or 1).
* group_dec_rt – Time from the onset of the 'group decisions required' promt to the input of the decision.
* group_dec_time – Time from the start of the experiment to the input of the decision.
* group_dec_client – The identifier of the client (workstation) used to input the group decision.
* trial_finish - Time (in seconds) from the start of the experiment to the end of this trial (i.e. offset of feedback).

Following names represent columns specific to a single participant. They are of the form pX_name, where 'X' stands for the participant index, and 'name' is one of listed below:

* indiv_dec – Participant's decision in this trial (0 or 1)
* indiv_dec_rt - Time from the onset of the individual decisions promt ('?') to the input of the decision.
* indiv_dec_time - Time from the start of the experiment to the input of the decision.
* client – Id of the client (workstation) used by this participant.

Following names represent columns specific to a particualr stimulus (screen) being presented. They are of the form pX_name.parameter, where 'X' stands for the participant index, on whose workstation the screen was presented, 'name' is the name of the stimulus (screen), and 'parameter' is the relevant parameter name.

The screens are:

* trial_wait – Blank screen preceeding the stimuli.
* fixation – Black cross preceeding the stimuli.
* stim1 – The first set of Gabor patches.
* blank_between – Blank screen between the two sets of patches.
* stim2 - The second set of Gabor patches.
* indiv_dec_prompt – A "?", aigni indicating a request for individual decisions. Disappears after the decision has been made.
* group_dec_prompt – A request for a group decision. Disappears after the decision has been made.

The parameters are:

* onset – Time (in seconds) from the start of the experiment, to the moment, when the stimulus apperas on the screen.
* offset - Time (in seconds) from the start of the experiment, to the moment, when the stimulus disapperas from the screen.
* duration - Duration in which the stimulus is visible.

## Survey file (nminds-x4-survey.csv) ##

Column descriptions:

* session_id - Id of the session, as a number.
* client - A letter identifier of the client (workstation) used by this participant.
* sex - The participant's self-reported gender - m(ale) or f(emale).
* age - Age of the participant (self-reported).
* edu - Education level of the participant ona scale 1-5 (for details see the main report).
* knows_left, knows_front, knows_right - The extent to which the participant is familiar with the other group members - the one to hers (his) left, front or right respectively. The scale is 1-4, for details see the main report.